import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

for m in [10,20]:

    for trial in [0,1,2,3]:
    
        data = np.load('errors_m{}_t{}.npz'.format(m,trial))
        e = data['errors']
        N = data['N']
        
        for n in range(e.shape[1]):
            ee = e[:,n,:]
        
            plt.figure(figsize=(2.5,2.5))
            plt.loglog(N, ee, 'ko-')
            plt.xlabel('Number of samples')
            plt.ylabel('Error')
            plt.ylim(1e-4, 1e1)
            plt.xlim(1e1, 1e5)
            #plt.title('m={}, n={}'.format(m, n+2))
            
            figname = 'figs/tr{}/n{}m{}.eps'.format(trial, n+2, m)
            plt.savefig(figname, dpi=300, bbox_inches='tight', pad_inches=0.0)
        
        
    plt.show()

