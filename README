Scripts for running the numerical experiments in:
'A randomized algorithm for enumerating zonotope vertices'
K. Stinson, D.F. Gleich, and P.G. Constantine

The following code runs studies similar to those in the paper:

python zonotope_driver.py W0m10.npz
python zonotope_driver.py W0m20.npz
python zonotope_driver.py W1m10.npz
python zonotope_driver.py W1m20.npz
python zonotope_driver.py W2m10.npz
python zonotope_driver.py W2m20.npz

To increase the number of samples, change the 'Nsamples' variable in the 
scripts. Beware that increasing the number of samples or the number of trials 
to that in the paper will increase the compute time significantly. 

Once that data is generated as .npz files, run:

python error_plots.py
python stops_plots.py

to create Figures 4 and 5. 

The code to run the timing study in Table 1 is:

python timing_zonotope_codes.py

The code requires that one install libzonotope 
(https://github.com/dgleich/libzonotope) and get the python wrapper working.
