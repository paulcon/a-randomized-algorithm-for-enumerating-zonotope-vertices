import numpy as np
import zonotopes as zn
import sys
import pdb

if __name__=='__main__':
    m = int(sys.argv[1])
    ind = np.random.randint(1000)
    maxn = 5
    W = np.linalg.qr(np.random.normal(size=(m,maxn)))[0]
    Ntrials = 10
    
    # allocate memory
    Nsamples = 1e5
    N = np.round(np.logspace(1, np.log10(Nsamples), 9)).astype(int)
    errors = np.zeros((len(N), maxn-1, Ntrials))

    for n in np.arange(2, maxn+1):
        truth = zn.zonotope_vertices(W[:,0:n], Nsamples=1e4, maxcount=1e6)
        for j in range(Ntrials):
            Z = np.random.normal(size=(Nsamples, n))
            errors[:,n-2,j] = zn.zonotope_haus_errors2(W[:,0:n], Z, truth)[0]
        np.savez('errors_m{:d}_ind{:d}'.format(m,ind), W=W, errors=errors, N=N)
    
    
    
    
    
