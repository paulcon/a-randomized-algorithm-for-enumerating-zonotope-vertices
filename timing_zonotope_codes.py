import numpy as np
import zonotopes as zran
import zonotopes_revsearch as zrev
import time as tt

ms = [5, 10, 15, 20]
ns = [2, 3]
Ntimes = 1000

revtimes = np.zeros((len(ms), len(ns), Ntimes))
rantimes = np.zeros((len(ms), len(ns), Ntimes))

np.random.seed(42)
counti = 0
for m in ms:
    
    countj = 0
    for n in ns:
        
        # make a random orthogonal matrix of generators
        A = np.random.normal(size=(m, n))
        W = np.linalg.qr(A)[0]
        WW = np.vstack((W, -W))
        
        countk = 0
        for k in range(Ntimes):
            t0 = tt.clock()
            Vrev = np.array(zrev.zonotope_vertices(WW))
            t1 = tt.clock()
            revtimes[counti,countj,countk] = t1-t0
            
            t0 = tt.clock()
            Vran = zran.zonotope_vertices(W, Nsamples=1e4, maxcount=1e6)
            t1 = tt.clock()
            rantimes[counti,countj,countk] = t1-t0
            
            if not np.mod(k,100):
                print '==== k {} ===='.format(countk)
                print 'Rev({},{}): {:06.4f} secs'.format(m, n, revtimes[counti,countj,countk])
                print 'Ran({},{}): {:06.4f} secs'.format(m, n, rantimes[counti,countj,countk])
            
            countk += 1
            
        countj += 1
        
    counti += 1

np.savez('times.npz', revtimes=revtimes, rantimes=rantimes)
