import numpy as np
import zonotopes as zn
import sys
import pdb

if __name__=='__main__':
    fname = sys.argv[1]
    ff = np.load(fname)
    W = ff['W']
    m = W.shape[0]
    
    Ntrials = int(1e1)
    
    # allocate memory
    Nsamples = 1e5
    area_errors = np.zeros((Nsamples, Ntrials))
    area_Nstops = np.zeros((1, Ntrials))    

    # 2d
    n = 2
    #Y = zn.zonotope_vertices(W[:,0:n])
    #truth = zn.polyarea(Y)
    truth = zn.zonotope_vertices(W[:,0:n])
    for j in range(Ntrials):
        Z = np.random.normal(size=(Nsamples, n))
        #area_errors[:,j], area_Nstops[0,j] = zn.zonotope_errors(W[:,0:n], Z, truth)
        area_errors[:,j], area_Nstops[0,j] = zn.zonotope_haus_errors(W[:,0:n], Z, truth)
    
    np.savez('errors2d_{}'.format(fname), W=W, area_errors=area_errors)
    np.savez('nstops2d_{}'.format(fname), W=W, area_Nstops=area_Nstops)

    # allocate memory
    Nsamples = 1e5
    vol_errors = np.zeros((Nsamples, Ntrials))
    vol_Nstops = np.zeros((1, Ntrials))
        
    # 3d
    n = 3
    #Y = zn.zonotope_vertices(W[:,0:n])
    #truth = zn.convex_hull_volume(Y)
    truth = zn.zonotope_vertices(W)
    for j in range(Ntrials):
        Z = np.random.normal(size=(Nsamples, n))
        #vol_errors[:,j], vol_Nstops[0,j] = zn.zonotope_errors(W, Z, truth)
        vol_errors[:,j], vol_Nstops[0,j] = zn.zonotope_haus_errors(W, Z, truth)
    
    np.savez('errors3d_{}'.format(fname), W=W, vol_errors=vol_errors)
    np.savez('nstops3d_{}'.format(fname), W=W, vol_Nstops=vol_Nstops)
    
    
    
    
