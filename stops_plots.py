import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

max_trials = 2000

for m in [10,20]:

    for trial in [0,1,2]:
        
        d = np.load('nstops2d_W{}m{}.npz'.format(trial,m))
        N = np.trim_zeros(d['area_Nstops'][0])
        bins = np.logspace(0,8,30)
        h = np.histogram(N, bins)
        
        plt.figure(figsize=(3,3))
        plt.bar(h[1][:-1], h[0], 0.5*(h[1][1:]-h[1][:-1]))
        plt.gca().set_xscale("log")
        plt.xlabel('Number of samples')
        plt.ylabel('Histogram')
        figname = 'figs/stops_n2m{}tr{}.eps'.format(m,trial)
        plt.savefig(figname, dpi=300, bbox_inches='tight', pad_inches=0.0)
        
        d = np.load('nstops3d_W{}m{}.npz'.format(trial,m))
        N = np.trim_zeros(d['vol_Nstops'][0])
        bins = np.logspace(0,8,30)
        h = np.histogram(N, bins)
        
        plt.figure(figsize=(3,3))
        plt.bar(h[1][:-1], h[0], 0.5*(h[1][1:]-h[1][:-1]))
        plt.gca().set_xscale("log")
        plt.xlabel('Number of samples')
        plt.ylabel('Histogram')
        figname = 'figs/stops_n3m{}tr{}.eps'.format(m,trial)
        plt.savefig(figname, dpi=300, bbox_inches='tight', pad_inches=0.0)
        
    plt.show()
